<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <style>
        #pop_up {
            display: none;
        }

        #pop_up p {
            text-align: center;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <button id="sync" class="btn btn-primary">SYNC</button>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Currency</th>
                    <th>Value</th>
                    <th>Modified</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>

            <div id="pop_up" class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close">&times;</button>
                    <h4 class="modal-title">Edit Currency Value</h4>
                </div>
                <p></p>
                <input type="text" class="form-control">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default">Edit</button>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/currency.js') }}"></script>
</body>
</html>
