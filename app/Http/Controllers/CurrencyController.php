<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;

class CurrencyController extends Controller
{
    public function index()
    {
        return view('currency');
    }

    /**
     * @param Request $request
     */
    public function create(Request $request)
    {
        if (!$request->ajax()) {
            return;
        }

        $currencies = json_decode($request->getContent(), true);

        foreach ($currencies as $currency => $value) {

            $data = Currency::select('id')->where('currency', $currency)->get();

            if (!count($data)) {

                $currencyTable = new Currency();
                $currencyTable->currency = $currency;
                $currencyTable->value = $value;

                $currencyTable->save();

                continue;
            }

            $this->updateData($currency, $value);
        }
     }

    /**
     * @param Request $request
     */
     public function update(Request $request)
     {
         if (!$request->ajax()) {
             return;
         }

         $currency= json_decode($request->getContent(), true);
         $this->updateData($currency['currency'], $currency['value']);
     }

    /**
     * @param $currency
     * @param $value
     */
     private function updateData($currency, $value)
     {
         Currency::where('currency', $currency)
             ->update(['value' => $value]);
     }
}
